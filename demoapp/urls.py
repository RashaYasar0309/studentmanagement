from . import views
from django.urls import path

urlpatterns = [
    path('', views.dashboard_view, name='dashboard'),
    path('dashboard/', views.dashboard_view, name='dashboard'),
    path('profile/<int:id>', views.profile_view, name='profile'),
    path('student_add/', views.studentadd_view, name='student_add'),
    path('student_list/', views.studentList_view, name='student_list'),
    path('login/', views.login_view, name='login'),
    path('logout/', views.logout_view, name='logout'),
    path('add_mark/', views.add_mark_view, name='add_mark'),
    path('edit_user/', views.edit_user_view, name='edit_user'),
    path('user_delete/<int:id>/', views.user_delete_view, name='user_delete'),
]
