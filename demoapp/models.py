from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
import uuid
from datetime import date
# Create your models here.


class UserManager(BaseUserManager):

    def create_user(self, email, password=None, **kwargs):
        if not email:
            raise ValueError('Enter your email')
        account = self.model(email=self.normalize_email(email), **kwargs)
        account.set_password(password)
        account.save(using=self._db)
        return account

    def create_superuser(self, email, password):
        account = self.create_user(email, password=password)
        account.is_superuser = True
        account.is_staff = True
        account.save(using=self._db)
        return account


ROLE_CHOICES = [
    (1, 'Admin'),
    (2, 'Student'),

]


class Users(AbstractBaseUser, PermissionsMixin):
    uuid = models.UUIDField(
        # primary_key=True,
        default=uuid.uuid4,
        editable=False)
    name = models.CharField(max_length=100)
    role = models.IntegerField(choices=ROLE_CHOICES, null=True)
    dob = models.DateField(blank=True, null=True)
    email = models.CharField(max_length=100, unique=True)
    phone = models.CharField(max_length=50, unique=True)
    image = models.ImageField(upload_to='photo/')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    USERNAME_FIELD = 'email'

    REQUIRED_FIELDS = []

    objects = UserManager()

    def get_age(self):
        today = date.today()
        return today.year - self.dob.year - \
            ((today.month, today.day) < (self.dob.month, self.dob.day))

    def __str__(self):
        return self.name

# model mark


class Mark(models.Model):
    mark = models.FloatField(default=0)
    student = models.ForeignKey(Users, on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.student
