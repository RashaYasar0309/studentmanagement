from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.contrib.auth import login, logout, authenticate
from django.db.models import ProtectedError
from django.contrib.auth.decorators import login_required


from .models import (
    Users,
    Mark
)

# login view


def login_view(request):

    msg = ''
    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')
        print(email, password)
        Users = authenticate(email=email, password=password)
        if Users:
            login(request, Users)
            messages.success(request, 'Login successfully')
            return redirect('dashboard')
        else:
            messages.error(request, 'Invalid credentials')
    return render(request, 'login.html',)

# logout view


def logout_view(request):
    logout(request)
    return redirect('login')

# Dashboard view


@login_required
def dashboard_view(request):
    context = {
        'user': Mark.objects.filter(student__role=2).order_by('-created_at'),

    }
    return render(request, 'dashboard.html', context=context)

# student add view


@login_required
def studentadd_view(request):
    msg = ''
    if request.method == 'POST':
        name = request.POST.get('u_name')
        email = request.POST.get('u_email')
        password = request.POST.get('u_password')
        user_type = request.POST.get('user_type')
        phone = request.POST.get('u_phone')
        confirm_password = request.POST.get('u_crmPassword')
        dob = request.POST.get('u_dob')
        photo = request.FILES['photo']
        email_check = Users.objects.filter(email=email)
        if not email_check:
            mobile_check = Users.objects.filter(phone=phone)
            if not mobile_check:
                if password == confirm_password:
                    user = Users.objects.create_user(
                        name=name,
                        role=user_type,
                        dob=dob,
                        email=email,
                        password=password,
                        phone=phone,
                        image=photo

                    )
                    user.save()
                    msg = 'success'
                    messages.success(request, 'user added successfull')
                    obj = Users.objects.get(name=name)
                    mark_obj = Mark.objects.create(student=obj)
                    mark_obj.save()
                    return redirect('student_add')
                    # settings = Setting.objects.create(fk_user=account)
                    # settings.save()
                else:
                    messages.success(request, 'password_mismatch')
                    msg = 'password_mismatch'
            else:
                messages.success(request, 'mobile_duplicate')
                msg = 'mobile_duplicate'
        else:
            messages.success(request, 'email_duplicate')
            msg = 'email_duplicate'

    return render(request, 'student_add.html',)

# student list view


@login_required
def studentList_view(request):
    context = {
        'user': Mark.objects.filter(student__role=2).order_by('-created_at'),

    }
    return render(request, 'student_list.html', context=context)


@login_required
def add_mark_view(request):
    if request.method == 'POST':
        student_id = request.POST.get('student_id')
        mark = request.POST.get('mark')
        student_obj = Users.objects.get(id=student_id)
        mark_obj = Mark.objects.get(student=student_obj)
        mark_obj.mark = mark
        mark_obj.save()
        msg = 'success'
        if msg == 'success':
            messages.success(request, 'Mark has been added ')
        else:
            messages.warning(request, 'Mark not added ')
    return redirect('student_list')


# user edit view


@login_required
def edit_user_view(request):
    if request.method == 'POST':
        id = request.POST.get('studentid')
        name = request.POST.get('name')
        email = request.POST.get('email')
        mobile_no = request.POST.get('mobile_no')
        if name:
            user = Users.objects.get(id=id)
            user.name = name
            user.email = email
            user.phone = mobile_no
            user.save()
            messages.success(request, 'student has been updated ')
        else:
            messages.warning(request, 'student has not updated')

    return redirect('student_list')

# student delete view


@login_required
def user_delete_view(request, id):
    user = get_object_or_404(Users, id=id)

    try:
        user.delete()
        messages.success(request, 'User has been deleted.')
        return redirect('user_list')
    except ProtectedError:
        messages.warning(
            request,
            "User has not been deleted.Reference exists.")
        return redirect('user_list')

# profile view


@login_required
def profile_view(request, id):
    user = get_object_or_404(Users, id=id)
    if request.method == 'POST':
        current_password = request.POST.get('password')
        if current_password == user.password:

            password = request.POST.get('newpassword')
            confirm_password = request.POST.get('renewpassword')
            if password == confirm_password:
                user.set_password(password)
                user.save()

                messages.info(
                    request, 'Password has been updated, Please login to continue')
                return redirect('logout')

            else:
                messages.warning(request, 'Passwords are not matching')
                return redirect('profile', id=id)
        else:
            messages.warning(request, 'Current Passwords is not matching')
            return redirect('profile', id=id)

    context = {'usr': user, 'header': 'Change Password', }

    return render(request, 'profile.html', context)
